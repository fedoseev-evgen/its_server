const users = require('../../scheme/users');
const config = require ('../../libs/config');
const log = require('../../libs/logs')(module);

module.exports=function (req, res) {
    try{
  users.findOneAndUpdate({email:req.params.email,hex:req.params.hex},{'$set':{status:true}},(err, result) => {
    if (err) {
        log.error(err)
        res.send ("Похоже ссылка повреждена.");
    }else
        if(result===null){
            log.info("Почта: " + req.params.email + "НЕ смогла подтвердиться.");
          res.send ("Похоже ссылка повреждена");
        }else{
            log.info("Почта: " + req.params.email + "была подтверждена для подписки.");
            res.redirect(config.outIP+"/#/message/1");
        }
  });
}catch(e){
    log.error(e);
    res.send("0Ошибка");
}
}