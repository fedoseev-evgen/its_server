const project = require('../../scheme/project');
const config = require('../../libs/config');
const saveFile = require('../../libs/saveFiles')

const log = require('../../libs/logs')(module);

module.exports = async function (req, res) {
    log.info(req.body);
    try {
        var newStudent = new project(
            req.body
        );
        newStudent.save(function (err, user) {
            if (err) {
                log.error(err);
                res.status(500).send("Ошибка");
            } else {
                var mass = [];
                if (req.files !== undefined)
                    if (req.files[0] !== undefined) {
                        saveFile('/', req.files[0], user._id + "-=0", (err, file) => {
                            // if(err) res.send("1Запрос успешно отправлен");
                            mass.push(file);
                            user.set({fileArray: mass})
                            user.save(() => {
                                if (req.files[1] !== undefined) {
                                    saveFile('/', req.files[1], user._id + "-=1", (err, file) => {
                                        // if(err) res.send("1Запрос успешно отправлен");
                                        mass.push(file);
                                        user.set({fileArray: mass})
                                        user.save(() => {
                                            if (req.files[2] !== undefined) {
                                                saveFile('/', req.files[2], user._id + "-=2", (err, file) => {
                                                    // if(err) res.send("1Запрос успешно отправлен");
                                                    mass.push(file);
                                                    user.set({fileArray: mass})
                                                    user.save(() => {
                                                        if (req.files[3] !== undefined) {
                                                            saveFile('/', req.files[3], user._id + "-=3", (err, file) => {
                                                                // if(err) res.send("1Запрос успешно отправлен");
                                                                mass.push(file);
                                                                user.set({fileArray: mass})
                                                                user.save(() => {
                                                                    if (req.files[4] !== undefined) {
                                                                        saveFile('/', req.files[4], user._id + "-=4", (err, file) => {
                                                                            if (err) res.send("1Запрос успешно отправлен");
                                                                            mass.push(file);
                                                                            user.set({fileArray: mass})
                                                                            user.save(() => {
                                                                                res.send("Запрос успешно отправлен")
                                                                            });
                                                                        })
                                                                    } else res.send("Запрос успешно отправлен")
                                                                });
                                                            })
                                                        } else res.send("Запрос успешно отправлен")
                                                    });
                                                })
                                            } else res.send("Запрос успешно отправлен")
                                        });
                                    })
                                } else res.send("Запрос успешно отправлен")
                            });
                        })
                    } else res.send("Запрос успешно отправлен")
            }
        });
    } catch (e) {
        log.error(e)
        res.status(500).send("Ошибка");
    }
}