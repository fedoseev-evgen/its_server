const project = require('../../scheme/project');
const log = require('../../libs/logs')(module);

module.exports=function (req, res) {
  try{
    project.findByIdAndDelete(req.body._id,(err,result)=>{
      if(err||result===null){
          res.send("0Бриф не найден");
      }else{
        result.mess = "Удаление проекта"
      log.info(result);
        res.send("1Бриф удален");
      }
  })
}catch(e){
  log.error(e)
  res.send("0Ошибка");
}
}