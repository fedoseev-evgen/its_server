const users = require('../../scheme/users');
const config = require('../../libs/config');
const log = require('./../../libs/logs')(module);
const fs = require('fs');

module.exports=function (req, res) {
    fs.readFile(__dirname+"/../../logs/allLog.log", "utf8", 
    function(error,data){
        if(error) res.send("[]"); // если возникла ошибка
        else {
            data = data.replace(/\n/g, ",");
            data = data.substr(0,data.length-1);//Надо более полно продумать этот момент, чтобы убрать именно поледнюю запятую
            res.send("["+data+"]");
        } 
    });
}