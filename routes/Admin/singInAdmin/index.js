const Admin = require('../../../scheme/admin');
const jwt = require('jsonwebtoken');
const config = require('../../../libs/config');
const log = require('../../../libs/logs')(module);

module.exports = function (req, res) {
    log.info("Попытка авторизации с IP: " + req.connection.remoteAddress + " (IP с кодировкой)")
    try{
    Admin.findOne({name: req.body.name}).exec()
        .then(payload => {
            if (payload === null) {
                throw Error('0Такая электронная почта не зарегистрирована!');
            } else
            if (payload.encryptPassword(req.body.password) !== payload.hashPassword) {
                throw Error('0Неверный пароль!');
            } else
                return payload;
        })
        .then(payload => {
            const refreshToken = jwt.sign({type: 'refresh'}, config.jwt.secretOrKey, { expiresIn: config.jwt.expiresInRefreshToken });
            Admin.update({ name: payload.name }, { $set: {token: refreshToken} }, (err) => {
                if (err) log.error(err);
            });
            const tokenInfo = {
                _id: payload._id,
                name: payload.name,
                token: refreshToken
            };
            const token = jwt.sign(tokenInfo, config.jwt.secretOrKey, {expiresIn: config.jwt.expiresIn});
            log.info("Успешная авторизация с IP: " + req.connection.remoteAddress + " (IP с учетом кодировки)")
            res.send({token:token});
        })
        .catch(err => {
            log.error(err);
            res.send("0"+err.message);
        });
    }catch(e){
        log.error(e)
        res.send("0Ошибка");
    }
};
