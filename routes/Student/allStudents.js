const student = require('../../scheme/student');
const log = require('../../libs/logs')(module);

module.exports=function (req, res) {
  try{
  if(req.body.type!==undefined){
    student.find({type:req.body.type,status:true}).exec((err, result) => {
      if (err) {
          log.error(err)
          res.send ([])
      }else
          if(result===null){
            res.send ([])
          }else{
            res.send (result)
          }
    });
  }else{res.send([])}
}catch(e){
  log.error(e)
  res.send("0Ошибка");
}
}