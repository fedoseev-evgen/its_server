const mongoose = require('mongoose');
const crypto = require('crypto');
const usersSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    name:{
        type:String,
        required:true,
        validate: {
            validator: function(text) {
                return text.length>=1&&text.length<=30;
            },
            message: 'Имя не прошло валидацию'
        }
    },
    email:{
        type:String,
        required:true,
        validate: {
            validator: function(text) {
                return text.length>=5&&text.length<=60;
            },
            message: 'Email не прошёл валидацию'
        }
    },
    salt:{
        type:String,
    },
    hex:{
        type:String
    },
    status:{
        type:Boolean,
        default:false,
    },
    date:{
        type:Date,
        default:new Date(),
    },
    type:{
        type:String,
        required:true,
        enum : ['frontend','backend', 'design','ios','android','ar','op','ai'],
    }
},{
    collection: "users",
    versionKey: false
});

usersSchema.virtual('email2')
    .set(function (email2) {
        this.salt = Math.random() + 'ahhhsdasd';
        this.hex = this.encryptEmail2(email2);
    });

    usersSchema.methods = {
        encryptEmail2: function (email2) {
            return crypto.createHmac('sha256', this.salt).update(email2).digest('hex');
        },
        checkEmail2: function (email2) {
            return this.encryptEmail2(email2) === this.hex;
        }
    };  

    usersSchema.statics = {
        checkEmail:async function (email) {
            const _email = await this.findOne({email: email}).exec();
            if (_email === null){
                return true;
            } else {
                return false;
            }
        }
    }    

usersSchema.pre('save', function(next) {
    if(this._id===null||this._id===undefined)
    this._id = new mongoose.Types.ObjectId();



    next();
});

module.exports = mongoose.model('users', usersSchema);