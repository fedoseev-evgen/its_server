const mongoose = require('mongoose');
const brifSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    name:{
        type:String,
        required:true,
        validate: {
            validator: function(text) {
                return text.length>=1&&text.length<=30;
            },
            message: 'Имя не прошло валидацию'
        }

    },
    lastName:{
        type:String,
        required:true,
        validate: {
            validator: function(text) {
                return text.length>=1&&text.length<=30;
            },
            message: 'Фамилия не прошло валидацию'
        }
    },
    email:{
        type:String,
        required:true,
        validate: {
            validator: function(text) {
                return text.length>=5&&text.length<=60;
            },
            message: 'Email не прошёл валидацию'
        }
    },
    num:{
        type:String,
        required:true,
        validate: {
            validator: function(text) {
                return text.length>=6&&text.length<=30;
            },
            message: 'Номер не прошёл валидацию'
        }
    },
    course:{
        type:String,
        required:true,
        validate: {
            validator: function(text) {
                return text.length>=1&&text.length<=30;
            },
            message: 'Курс/школа не прошли валидацию'
        }
    },
    date:{
        type:Date,
        default:new Date()
    },
    idea:{
        type:String,
        required:true,
        validate: {
            validator: function(text) {
                return text.length>=10&&text.length<=5000;
            },
            message: 'Идея не прошла валидацию'
        }
    },
    team:{
        type:String,
        required:true,
        validate: {
            validator: function(text) {
                return text.length>=1&&text.length<=5000;
            },
            message: 'Команда не прошла валидацию'
        }
    },
    term:{
        type:String,
        required:true,
        validate: {
            validator: function(text) {
                return text.length>=3&&text.length<=5000;
            },
            message: 'Срок не прошёл валидацию'
        }
    },
    fileArray:{
        type:Array,
        default:[]
    }
},{
    collection: "brifSchema",
    versionKey: false
});



brifSchema.pre('save', function(next) {
    if(this._id===null||this._id===undefined)
    this._id = new mongoose.Types.ObjectId();
    next();
});

module.exports = mongoose.model('brifSchema', brifSchema);