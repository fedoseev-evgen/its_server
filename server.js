﻿const express = require('express');
const bodyParser = require('body-parser');
const http = require('http');
const https = require('https');
const compression = require('compression');
const multer = require('multer');
const mongoose = require('mongoose');
const cookieParser = require('cookie-parser');
const path = require('path');
const chekMail = require('./libs/chekMail');
const ITS_TABEL = require('./libs/ITS_TABEL');
const config = require('./libs/config');
const log = require('./libs/logs')(module);
const fs = require('fs');


mongoose.connect(config.mongoose.url, { useNewUrlParser: true , useCreateIndex: true }, function (err) {
if (err) 
    return console.log(err);  

    // const options = {
    //     key: fs.readFileSync('/etc/letsencrypt/live/it-student.ru/privkey.pem'),
    //     cert: fs.readFileSync('/etc/letsencrypt/live/it-student.ru/cert.pem')
    //   };

const app = express();
app.use(cookieParser());
app.use(compression());
app.set('view engine', 'ejs');
app.use('/', express.static('./public/its'));
app.use('/files', express.static('./files'));
app.use(bodyParser.urlencoded({
    extended: false,
    limit: '50mb'
}));
app.use(multer(
    {
        dest: path.join(__dirname, 'public/uploads'),
        limits: {
            files:5,
        }
    }
).any());

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Credentials", "true");
    res.header("Access-Control-Allow-Headers", "Content-Type");
    next();
});

setInterval(chekMail,1000*60*60);
setInterval(ITS_TABEL,1000*60*60);
require('./routes')(app);
app.use(function(req, res){
    res.send('404 ошибка');
});
var httpServer = http.createServer(app);
function onListening(){
    log.info('Listening on port ', config.port);
}
    httpServer.on('listening', onListening);
    httpServer.listen(config.port, '0.0.0.0');
});
